/*
	IAM untuk Kubernetes Master
*/
# Creating iam role for master
resource "aws_iam_role" "kube-iam-role-master" {
	name 					= "${var.iam-profile-master}-${var.cluster-name}"
	assume_role_policy		= "${template_file.master-iam-role.rendered}"
}

# Creating iam policy for master
resource "aws_iam_role_policy" "kube-iam-policy-master" {
	name 					= "${var.iam-profile-master}-${var.cluster-name}"
	role 					= "${aws_iam_role.kube-iam-role-master.id}"
	policy 					= "${template_file.master-iam-policy.rendered}"
}

# Creating iam instance-profile master
resource "aws_iam_instance_profile" "kube-iam-instance-profile-master" {
	name 					= "${var.iam-profile-master}-${var.cluster-name}"
	roles 					= ["${aws_iam_role.kube-iam-role-master.name}"]
}

/*
	IAM untuk Kubernetes Minions
*/
# Creating iam role for minion
resource "aws_iam_role" "kube-iam-role-minion" {
	name 					= "${var.iam-profile-minion}-${var.cluster-name}"
	assume_role_policy		= "${template_file.minion-iam-role.rendered}"
}

# Creating iam policy for minion
resource "aws_iam_role_policy" "kube-iam-policy-minion" {
	name 					= "${var.iam-profile-minion}-${var.cluster-name}"
	role 					= "${aws_iam_role.kube-iam-role-minion.id}"
	policy 					= "${template_file.minion-iam-policy.rendered}"
}

# Creating iam instance-profile minion
resource "aws_iam_instance_profile" "kube-iam-instance-profile-minion" {
	name 					= "${var.iam-profile-minion}-${var.cluster-name}"
	roles 					= ["${aws_iam_role.kube-iam-role-minion.name}"]
}