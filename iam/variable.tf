/*
	Required Variables
*/
variable "cluster-name" {}


/*
	Other Variables
*/
variable "iam-profile-master" {
	default = "kube-master"
}

variable "iam-profile-minion" {
	default = "kube-minion"
}

