#! /bin/bash

# Pre-configuration
# ------------------
echo "--Check Prerequisite kubctl and terraform"

# Create Kube Config file
readonly KUBECONFIG="${HOME}/.kube/config"
if [[ ! -e "${KUBECONFIG}" ]]; then
    mkdir -p $(dirname "${KUBECONFIG}")
    touch "${KUBECONFIG}"
fi
echo "Using file '${KUBECONFIG}' as configuration file"

# Check apakah ada kubectl
command -v kubectl >/dev/null 2>&1 || \
{ echo >&2 "Please install kubectl! (see: http://kubernetes.io/v1.1/docs/getting-started-guides/aws.html#getting-started-with-your-cluster)"; exit 1; }

# Check apakah ada terraform
command -v terraform >/dev/null 2>&1 || \
{ echo >&2 "Please install terraform! (see: https://www.terraform.io/downloads.html)"; exit 1; }

# Execute Plan
# ------------------
echo "--Execute plan"
terraform apply

if [ ! $? -eq 0 ]; then
	# Jika gagal, langsung dihapus saja
	terraform destroy -force
    exit 1
fi

# Post Configuration
# ------------------
echo "--Post Configuration"

# Get Kube Master IP
readonly MASTER_IP_FILE="/tmp/kube-master-ip.tmp"
if [[ ! -e "${MASTER_IP_FILE}" ]]; then
    echo "Could not retrieve Kube Master IP (${MASTER_IP_FILE})"
    exit 1
fi
while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "Kube Master IP: $line"
    MASTER_IP=${line}
done < "${MASTER_IP_FILE}"

# Check SSH
readonly SSH_USER="ubuntu"
readonly SSH_KEY="$(pwd)/keypair/key.pem"
chmod 400 "${SSH_KEY}"

echo "Check SSH Connection"
attempt=0
  while true; do
    echo -n Attempt "$(($attempt+1))" to check for SSH to master
    ok=1
    output=$(ssh -oStrictHostKeyChecking=no -i "${SSH_KEY}" ${SSH_USER}@${MASTER_IP} uptime) || ok=0
    if [[ ${ok} == 0 ]]; then
      if (( attempt > 30 )); then
        echo "(Failed) output was: ${output}"
        exit 1
      fi
    else
      echo -e " ${color_green}[ssh to master working]${color_norm}"
      break
    fi
    echo -e " ${color_yellow}[ssh to master not working yet]${color_norm}"
    attempt=$(($attempt+1))
    sleep 10
  done

# Wait Cluster for settle
echo "--5 minutes wait for cluster to settle"
for (( i=0; i < 6*5; i++)); do
  printf "."
  sleep 10
done

# Update salt highstate
echo "Update salt highstate in Kube Master"
ssh -oStrictHostKeyChecking=no -i "${SSH_KEY}" ${SSH_USER}@${MASTER_IP} sudo salt '*' state.highstate

# Get Kube Master Certificate
export KUBE_CERT="/tmp/$RANDOM-kubecfg.crt"
export KUBE_KEY="/tmp/$RANDOM-kubecfg.key"
export CA_CERT="/tmp/$RANDOM-kubernetes.ca.crt"
export CONTEXT="aws_kubernetes"

ssh -oStrictHostKeyChecking=no -i "${SSH_KEY}" ${SSH_USER}@${MASTER_IP} sudo cat /srv/kubernetes/kubecfg.crt >"${KUBE_CERT}"
ssh -oStrictHostKeyChecking=no -i "${SSH_KEY}" ${SSH_USER}@${MASTER_IP} sudo cat /srv/kubernetes/kubecfg.key >"${KUBE_KEY}"
ssh -oStrictHostKeyChecking=no -i "${SSH_KEY}" ${SSH_USER}@${MASTER_IP} sudo cat /srv/kubernetes/ca.crt >"${CA_CERT}"

cluster_args=()
if [[ -z "${CA_CERT:-}" ]]; then
  cluster_args+=("--insecure-skip-tls-verify=true")
else
  cluster_args+=(
    "--certificate-authority=${CA_CERT}"
    "--embed-certs=true"
  )
fi

user_args+=(
     "--client-certificate=${KUBE_CERT}"
     "--client-key=${KUBE_KEY}"
     "--embed-certs=true"
    )

# Set configuration
echo "Export Kube Configuration"
kubectl config set-cluster "${CONTEXT}" "${cluster_args[@]}"
if [[ -n "${user_args[@]:-}" ]]; then
    kubectl config set-credentials "${CONTEXT}" "${user_args[@]}"
fi
kubectl config set-context "${CONTEXT}" --cluster="${CONTEXT}" --user="${CONTEXT}"
kubectl config use-context "${CONTEXT}"  --cluster="${CONTEXT}"

rm -f /tmp/*.tmp
echo "Wrote config for ${CONTEXT} to ${KUBECONFIG}"
