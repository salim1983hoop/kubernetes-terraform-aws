/*
	Create S3 Bucket untuk file-file yang dibutuhkan dalam proses instalasi.
*/
# Create S3
resource "aws_s3_bucket" "kube-s3" {
	bucket 					= "${template_file.s3-bucket-name.rendered}"
	policy					= "${template_file.s3-policy.rendered}"
}

# Upload staging file
resource "aws_s3_bucket_object" "kube-staging-server" {
	bucket 					= "${aws_s3_bucket.kube-s3.bucket}"
	key						= "${template_file.server-package-key.rendered}"
	source					= "${path.module}/staging/${var.server-package-name}"
}
resource "aws_s3_bucket_object" "kube-staging-salt" {
	bucket 					= "${aws_s3_bucket.kube-s3.bucket}"
	key						= "${template_file.salt-package-key.rendered}"
	source					= "${path.module}/staging/${var.salt-package-name}"
}

/*
	Load module Master dan Minion
*/
module "master" {
	source								= "master"

	aws-region							= "${var.aws-region}"
	cluster-name						= "${var.cluster-name}"
	cidr-block-prefix					= "${var.cidr-block-prefix}"
	instance-prefix						= "${var.instance-prefix}"
	service-cluster-ip-range			= "10.0.0.0/16"
	cluster-ip-range 					= "10.244.0.0/24"
	allocate-node-cidrs					= "true"
	server-package-url					= "${template_file.server-package-url.rendered}"
	salt-package-url					= "${template_file.salt-package-url.rendered}"
	enable-cluster-monitoring			= "influxdb"
	dns-server-ip					 	= "10.0.0.10"
	master-ip-range						= "10.246.0.0/24"
	num-minions							= "${var.num-minions}"

	master-disk-size					= "${var.master-disk-size}"
	master-disk-type					= "${var.master-disk-type}"
	master-instance-type				= "${var.master-instance-type}"
	master-subnet-id					= "${var.master-subnet-id}"
	master-sg-id						= "${var.master-sg-id}"

	ami-id								= "${var.ami-id}"
	iam-instance-profile-master			= "${var.iam-instance-profile-master}"
	ssh-key-name						= "${var.ssh-key-name}"
}

module "minion" {
	source								= "minion"

	cluster-name						= "${var.cluster-name}"
	instance-prefix						= "${var.instance-prefix}"
	master-internal-ip					= "${module.master.master-internal-ip}"
	ami-id								= "${var.ami-id}"
	num-minions							= "${var.num-minions}"

	iam-instance-profile-minion			= "${var.iam-instance-profile-minion}"

	ssh-key-name					 	= "${var.ssh-key-name}"

	minion-disk-size					= "${var.minion-disk-size}"
	minion-disk-type					= "${var.minion-disk-type}"
	minion-instance-type				= "${var.minion-instance-type}"
	minion-subnet-ids					= "${var.minion-subnet-ids}"
	minion-sg-id						= "${var.minion-sg-id}"
	spot-price							= "${var.spot-price}"
}