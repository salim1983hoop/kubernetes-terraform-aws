/*
	Required Variables
*/
variable "cluster-name" {}
variable "instance-prefix" {}
variable "master-internal-ip" {}
variable "ami-id" {}
variable "num-minions" {}

variable "minion-instance-type" {}
variable "iam-instance-profile-minion" {}

variable "ssh-key-name" {}

variable "minion-disk-size" {}
variable "minion-disk-type" {}
variable "minion-subnet-ids" {}
variable "minion-sg-id" {}
variable "spot-price" {}
