# Create Launch Configuration
resource "aws_launch_configuration" "node-lc" {
	name							= "${template_file.asg-name.rendered}"
	image_id						= "${var.ami-id}"
	iam_instance_profile			= "${var.iam-instance-profile-minion}"
	instance_type					= "${var.minion-instance-type}"
	key_name						= "${var.ssh-key-name}"
	security_groups					= ["${var.minion-sg-id}"]
	associate_public_ip_address 	= "true"
	enable_monitoring				= "false"
	user_data						= "${template_file.minion-start.rendered}"

	ebs_block_device {
		device_name					= "/dev/sda1"
		volume_size					= "${var.minion-disk-size}"
		volume_type					= "${var.minion-disk-type}"
		delete_on_termination		= "true"
	}

	ephemeral_block_device = {
		device_name				= "/dev/sdc"
		virtual_name			= "ephemeral0"
	}

	ephemeral_block_device = {
		device_name				= "/dev/sdd"
		virtual_name			= "ephemeral1"
	}

	ephemeral_block_device = {
		device_name				= "/dev/sde"
		virtual_name			= "ephemeral2"
	}

	ephemeral_block_device = {
		device_name				= "/dev/sdf"
		virtual_name			= "ephemeral3"
	}
}

# Create Auto Scaling Group
resource "aws_autoscaling_group" "kube-minion-asg" {
	name 						= "${template_file.asg-name.rendered}"
	launch_configuration 		= "${aws_launch_configuration.node-lc.id}"
	min_size					= "${var.num-minions}"
	max_size					= "${var.num-minions}"
	vpc_zone_identifier			= ["${split(",", "${var.minion-subnet-ids}")}"]
	wait_for_capacity_timeout	= "0"

	tag {
		key  					= "Name"
		value 					= "${template_file.node-instance-prefix.rendered}"
		propagate_at_launch   	= "true"
	}

	tag {
		key  					= "Role"
		value 					= "${var.instance-prefix}-minion"
		propagate_at_launch   	= "true"
	}	

	tag {
		key  					= "KubernetesCluster"
		value 					= "${var.cluster-name}"
		propagate_at_launch   	= "true"
	}	

	tag {
		key  					= "Application"
		value 					= "kubernetes"
		propagate_at_launch   	= "true"
	}	
}
