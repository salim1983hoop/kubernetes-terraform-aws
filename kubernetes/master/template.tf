resource "template_file" "master-internal-ip" {
	template = "${var.cidr-block-prefix}.0.9"
}


/*
	Bootstrap Kubernetes Master
*/
# Kubelet Token
variable "kubelet-token" {
	default = "4lmNSuRq1wkPeha09rk8659Y7etLTgb"
}

# Kubelet Proxy Token
variable "kubelet-proxy-token" {
	default = "mvJB9cBOw0bgLXGJqAQ9BKTs2CRsdj1l"
}

# Kubectl Context
resource "template_file" "kube-context" {
	template = "aws_${KUBE}"

	vars {
		KUBE 		= "${var.instance-prefix}"
	}
}

# Environment untuk Script instalasi Master
resource "template_file" "master-start-var" {
	template = "${file("${path.module}/scripts/master-start-var.sh")}"

	vars = {
		MASTER_INTERNAL_IP				= "${template_file.master-internal-ip.rendered}"
		INSTANCE_PREFIX					= "${var.instance-prefix}"
		NODE_INSTANCE_PREFIX			= "${var.instance-prefix}-minion"
		CLUSTER_IP_RANGE				= "${var.cluster-ip-range}"
		ALLOCATE_NODE_CIDRS				= "${var.allocate-node-cidrs}"
		SERVER_BINARY_TAR_URL			= "${var.server-package-url}"
		SALT_TAR_URL					= "${var.salt-package-url}"
		ZONE							= "${var.aws-region}a"
		KUBE_USER						= "${var.kube-user}"
		KUBE_PASSWORD					= "${var.kube-password}"
		SERVICE_CLUSTER_IP_RANGE		= "${var.service-cluster-ip-range}"
		ENABLE_CLUSTER_MONITORING		= "true"
		ENABLE_CLUSTER_LOGGING			= "true"
		ENABLE_NODE_LOGGING				= "true"
		LOGGING_DESTINATION				= "elasticsearch"
		ELASTICSEARCH_LOGGING_REPLICAS	= "1"
		ENABLE_CLUSTER_DNS				= "true"
		ENABLE_CLUSTER_UI				= "true"
		DNS_REPLICAS					= "1"
		DNS_SERVER_IP					= "${var.dns-server-ip}"
		DNS_DOMAIN						= "${var.dns-domain}"
		ADMISSION_CONTROL				= "NamespaceLifecycle,LimitRanger,SecurityContextDeny,ServiceAccount,ResourceQuota"
		MASTER_IP_RANGE					= "${var.master-ip-range}"
		KUBELET_TOKEN					= "${var.kubelet-token}"
		KUBE_PROXY_TOKEN				= "${var.kubelet-proxy-token}"
		DOCKER_STORAGE					= "aufs"
		MASTER_EXTRA_SANS				= "IP:10.0.0.1,DNS:kubernetes,DNS:kubernetes.default,DNS:kubernetes.default.svc,DNS:kubernetes.default.svc.${var.dns-domain},DNS:${var.instance-prefix}-master"
		NUM_MINIONS						= "${var.num-minions}"
	}
}

resource "template_file" "master-start" {
	template = "${ENV} \r\n \r\n${SCRIPT}"

	vars {
		ENV 			= "${template_file.master-start-var.rendered}"
		SCRIPT 			= "${file("${path.module}/scripts/master-start.sh")}"
	}
}