# Create Master Instance
resource "aws_instance" "kube-master-instance" {
	ami								= "${var.ami-id}"
	iam_instance_profile			= "${var.iam-instance-profile-master}"
	instance_type					= "${var.master-instance-type}"
	subnet_id						= "${var.master-subnet-id}"
	private_ip						= "${template_file.master-internal-ip.rendered}"
	key_name						= "${var.ssh-key-name}"
	vpc_security_group_ids			= ["${var.master-sg-id}"]
	associate_public_ip_address		= "true"
	user_data						= "${template_file.master-start.rendered}"

	ebs_block_device = {
		device_name				= "/dev/sda1"
		delete_on_termination	= "true"
		volume_size				= "${aws_ebs_volume.kube-ebs-master.size}"
		volume_type				= "${aws_ebs_volume.kube-ebs-master.type}"
	}

	ephemeral_block_device = {
		device_name				= "/dev/sdc"
		virtual_name			= "ephemeral0"
	}

	ephemeral_block_device = {
		device_name				= "/dev/sdd"
		virtual_name			= "ephemeral1"
	}

	ephemeral_block_device = {
		device_name				= "/dev/sde"
		virtual_name			= "ephemeral2"
	}

	ephemeral_block_device = {
		device_name				= "/dev/sdf"
		virtual_name			= "ephemeral3"
	}

	tags {
		Name 					= "${var.instance-prefix}-master-${var.cluster-name}"
		Application				= "kubernetes"
		Role					= "${var.instance-prefix}-master"
		KubernetesCluster		= "${var.cluster-name}"
	}
}

# Master Network Interface (elastic ip)
resource "aws_eip" "kube-eip-master" {
	vpc						= "true"
	instance 				= "${aws_instance.kube-master-instance.id}"

	provisioner "local-exec" {
		command 				= "echo '${aws_eip.kube-eip-master.public_ip}' > /tmp/kube-master-ip.tmp"
	}

	provisioner "local-exec" {
		command					= "kubectl config set-cluster ${template_file.kube-context.rendered} --server=https://${aws_eip.kube-eip-master.public_ip}"
	}

	provisioner "local-exec" {
		command					= "kubectl config set-credentials ${template_file.kube-context.rendered} --username=${var.kube-user}"
	}		

	provisioner "local-exec" {
		command					= "kubectl config set-credentials ${template_file.kube-context.rendered} --password=${var.kube-password}"
	}		
}

# Master Disk Storage
resource "aws_ebs_volume" "kube-ebs-master" {
	availability_zone 		= "${var.aws-region}a"
	size					= "${var.master-disk-size}"
	type					= "${var.master-disk-type}"

	tags {
		Name				= "kube-ebs-master"
		Application			= "kubernetes"
		KubernetesCluster	= "${var.cluster-name}"
	}
}

# Attach Volume
resource "aws_volume_attachment" "ebs-master-att" {
	device_name				= "/dev/sdb"
	volume_id				= "${aws_ebs_volume.kube-ebs-master.id}"
	instance_id				= "${aws_instance.kube-master-instance.id}"
	force_detach			= "true"
}

