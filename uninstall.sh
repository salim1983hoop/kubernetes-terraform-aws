#! /bin/bash

# Check apakah ada kubectl
command -v kubectl >/dev/null 2>&1 || \
{ echo >&2 "Please install kubectl! (see: http://kubernetes.io/v1.1/docs/getting-started-guides/aws.html#getting-started-with-your-cluster)"; exit 1; }

# Check apakah ada terraform
command -v terraform >/dev/null 2>&1 || \
{ echo >&2 "Please install terraform! (see: https://www.terraform.io/downloads.html)"; exit 1; }

echo "--Destroy plan"
terraform destroy -force

echo "--Unset kubectl configuration"
readonly CONTEXT="aws_kubernetes"
kubectl config unset "clusters.${CONTEXT}"
kubectl config unset "users.${CONTEXT}"
kubectl config unset "users.${CONTEXT}-basic-auth"
kubectl config unset "contexts.${CONTEXT}"

echo "--Remove unused files"
rm -f /tmp/*.crt
rm -f /tmp/*.key
