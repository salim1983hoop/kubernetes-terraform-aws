/*
	Required variable
*/
# AWS Credential dan preferable aws region
variable "aws-region" {}
variable "aws-access-key" {}
variable "aws-secret-key" {}

# AWS S3 Bucket Region
variable "aws-s3-bucket-region" {}

# Cluster Name
variable "cluster-name" {}

# Number of minions
variable "num-minions" {}

# Master Specification
variable "master-disk-size" {}
variable "master-disk-type" {}
variable "master-instance-type" {}

# Minion Specification
variable "minion-disk-size" {}
variable "minion-disk-type" {}
variable "minion-instance-type" {}
variable "spot-price" {}

# SSH Key
variable "ssh-key-name" {}

# AMI
variable "ami-id" {
	default = "ami-6c14310f"
}

/*
	Registered Modules
*/
module "network" {
	source				= "network"

	aws-region 			= "${var.aws-region}"
	cluster-name 		= "${var.cluster-name}"
}

module "iam" {
	source				= "iam"

	cluster-name 		= "${var.cluster-name}"	
}

module "security" {
	source				= "security"

	cluster-name 		= "${var.cluster-name}"	
	vpc-id				= "${module.network.vpc-id}"
}

module "kubernetes" {
	source								= "kubernetes"


	cluster-name 						= "${var.cluster-name}"	
	aws-region 							= "${var.aws-region}"
	aws-s3-region 						= "${var.aws-region}"
	bucket-name							= "kubernetes-staging-cluster-${var.cluster-name}"
	cidr-block-prefix					= "${module.network.cidr-block-prefix}"
	ami-id								= "${var.ami-id}"

	num-minions							= "${var.num-minions}"

	master-disk-size					= "${var.master-disk-size}"
	master-disk-type					= "${var.master-disk-type}"
	master-instance-type				= "${var.master-instance-type}"
	master-subnet-id				  	= "${module.network.kube-subnet-a-id}"
	master-sg-id						= "${module.security.kube-master-sg-id}"

	minion-disk-size					= "${var.minion-disk-size}"
	minion-disk-type					= "${var.minion-disk-type}"
	minion-instance-type				= "${var.minion-instance-type}"
	minion-subnet-ids				  	= "${module.network.kube-subnet-a-id},${module.network.kube-subnet-b-id}"
	minion-sg-id						= "${module.security.kube-minion-sg-id}"	
	spot-price							= "${var.spot-price}"

	iam-instance-profile-master			= "${module.iam.kube-iam-instance-profile-master-name}"
	iam-instance-profile-minion			= "${module.iam.kube-iam-instance-profile-minion-name}"

	ssh-key-name						= "${var.ssh-key-name}"
}
