/*
	VPC
*/
resource "aws_vpc" "kube-vpc" {
	cidr_block 				= "${template_file.vpc-cidr-block.rendered}"
	instance_tenancy 		= "default"
	enable_dns_hostnames 	= true

	tags {
		Name				= "kube-vpc"
		Application			= "kubernetes"
		KubernetesCluster	= "${var.cluster-name}"
	}
}

/*
	Subnet yang akan digunakan adalah subnet multi AZ.
*/
# Region A
resource "aws_subnet" "kube-subnet-a" {
	vpc_id					= "${aws_vpc.kube-vpc.id}"
	cidr_block 				= "${template_file.subnet-a-cidr-block.rendered}"
	availability_zone		= "${var.aws-region}a"
	tags {
		Name 				= "kube-subnet-a"
		Application			= "kubernetes"
		KubernetesCluster	= "${var.cluster-name}"
	}
}

# Region B
resource "aws_subnet" "kube-subnet-b" {
	vpc_id					= "${aws_vpc.kube-vpc.id}"
	cidr_block 				= "${template_file.subnet-b-cidr-block.rendered}"
	availability_zone		= "${var.aws-region}b"
	tags {
		Name 				= "kube-subnet-b"
		Application			= "kubernetes"
		KubernetesCluster	= "${var.cluster-name}"
	}
}

/*
	Route Table
	---
	Route table masih belum mempunyai nilai 'route'. Nilai tersebut
	akan tambahkan dalam proses pembuatan environment.
*/
resource "aws_route_table" "kube-rtb" {
	vpc_id					= "${aws_vpc.kube-vpc.id}"

	tags {
		Name 				= "kube-rtb"
		Application			= "kubernetes"
		KubernetesCluster	= "${var.cluster-name}"
	}
}

# Asosiasikan Route Table dengan subnet yang sudah dibuat
resource "aws_route_table_association" "a" {
	subnet_id				= "${aws_subnet.kube-subnet-a.id}"
	route_table_id			= "${aws_route_table.kube-rtb.id}"
}
resource "aws_route_table_association" "b" {
	subnet_id				= "${aws_subnet.kube-subnet-b.id}"
	route_table_id			= "${aws_route_table.kube-rtb.id}"
}

/*
	Internet Gate
	-----
	Digunakan agar vpc bisa koneksi ke ineternet
*/
resource "aws_internet_gateway" "kube-igw" {
	vpc_id					= "${aws_vpc.kube-vpc.id}"

	tags {
		Name 				= "kube-igw"
		Application			= "kubernetes"
		KubernetesCluster	= "${var.cluster-name}"
	}
}

# Daftarkan ke route table
resource "aws_route" "rtb-igw" {
	route_table_id				= "${aws_route_table.kube-rtb.id}"
	destination_cidr_block	 	= "0.0.0.0/0"
	gateway_id					= "${aws_internet_gateway.kube-igw.id}"
}