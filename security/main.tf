/*
	VPC Security Group Kubernetes Master
*/
# Security Group untuk Kubernetes Master
resource "aws_security_group" "kube-sg-master" {
	name					= "kube-sg-master"
	description				= "Kubernetes security group applied to master nodes"
	vpc_id					= "${var.vpc-id}"

	tags {
		Name				= "kube-sg-master-${var.cluster-name}"
		Application			= "kubernetes"
		KubernetesCluster	= "${var.cluster-name}"
	}

	egress {
		from_port 			= 0
		to_port				= 0
		protocol			= "-1"
		cidr_blocks			= ["0.0.0.0/0"]
	}
}

# Rule untuk SSH port ke master
resource "aws_security_group_rule" "master-allow-ssh" {
    type 							= "ingress"
    from_port 						= 22
    to_port 						= 22
    protocol 						= "tcp"
    cidr_blocks 					= ["0.0.0.0/0"]
    security_group_id 				= "${aws_security_group.kube-sg-master.id}"
}

# Rule SSL port ke master
resource "aws_security_group_rule" "master-allow-ssl" {
    type 							= "ingress"
    from_port 						= 443
    to_port 						= 443
    protocol 						= "tcp"
    cidr_blocks 					= ["0.0.0.0/0"]
    security_group_id 				= "${aws_security_group.kube-sg-master.id}"
}

# Rule untuk sg-master itu sendiri
resource "aws_security_group_rule" "master-allow-myself" {
    type 							= "ingress"
    from_port 						= 0
    to_port 						= 0
    protocol 						= "-1"
    source_security_group_id 		= "${aws_security_group.kube-sg-master.id}"
    security_group_id 				= "${aws_security_group.kube-sg-master.id}"
}

# Rule untuk sg-minions
resource "aws_security_group_rule" "master-allow-minions" {
    type 							= "ingress"
    from_port 						= 0
    to_port 						= 0
    protocol 						= "-1"
    source_security_group_id 		= "${aws_security_group.kube-sg-minions.id}"
    security_group_id 				= "${aws_security_group.kube-sg-master.id}"
}

/*
	VPC Security Group Kubernetes Minions
*/
# Security Group untuk Kubernetes Minion
resource "aws_security_group" "kube-sg-minions" {
	name					= "kube-sg-minions"
	description				= "Kubernetes security group applied to minions nodes"
	vpc_id					= "${var.vpc-id}"

	tags {
		Name				= "kube-sg-minions-${var.cluster-name}"
		Application			= "kubernetes"
		KubernetesCluster	= "${var.cluster-name}"
	}

	egress {
		from_port 			= 0
		to_port				= 0
		protocol			= "-1"
		cidr_blocks			= ["0.0.0.0/0"]
	}
}

# Rule untuk SSH port ke minions
resource "aws_security_group_rule" "minions-allow-ssh" {
    type 							= "ingress"
    from_port 						= 22
    to_port 						= 22
    protocol 						= "tcp"
    cidr_blocks 					= ["0.0.0.0/0"]
    security_group_id 				= "${aws_security_group.kube-sg-minions.id}"
}

# Rule untuk sg-minions itu sendiri
resource "aws_security_group_rule" "minions-allow-myself" {
    type 							= "ingress"
    from_port 						= 0
    to_port 						= 0
    protocol 						= "-1"
    source_security_group_id 		= "${aws_security_group.kube-sg-minions.id}"
    security_group_id 				= "${aws_security_group.kube-sg-minions.id}"
}

# Rule untuk sg-master
resource "aws_security_group_rule" "minions-allow-master" {
    type 							= "ingress"
    from_port 						= 0
    to_port 						= 0
    protocol 						= "-1"
    source_security_group_id 		= "${aws_security_group.kube-sg-master.id}"
    security_group_id 				= "${aws_security_group.kube-sg-minions.id}"
}