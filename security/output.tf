output "kube-master-sg-id" {
	value = "${aws_security_group.kube-sg-master.id}"
}

output "kube-minion-sg-id" {
	value = "${aws_security_group.kube-sg-minions.id}"
}